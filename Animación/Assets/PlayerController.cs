using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private Rigidbody rigidBody;
    private animator animator;
    private float hInput;
    private float vInput;
    private float runInput;
    public float currentSpeed = 0;
    public float walkSpeed = 3;
    public float runSpeed = 6;
    public float turnSmothing = 20f;

    public float idleWaitingTime = 5f;
    public float currentWaitingTime = 6;


    // Start is called before the first frame update
    void Start()
    {
        rigidBody = GetComponent<Rigidbody>();
        if (rigidBody == null)
            Debug.Log("No rigidbody component found in this gameobject");
        enabled = false;
        return;

        animator = GetComponent<Animator>();
        if (animator == null)
            Debug.Log("No animator component found in this gameobject");
        enabled = false;
        return;

    }

    // Update is called once per frame
    void Update()
    {
        hInput = Input.GetAxis("Horizontal");
        vInput = Input.GetAxis("Vertical");
        runInput = Input.GetAxis("Run");

        if (hInput != 0 || vInput != 0)
        {
            Quaternion newRotation = Quaternion.LookRotation(new Vector3(hInput, 0, vInput));
            transform.rotation = Quaternion.Slerp(transform.rotation, newRotation, Time.deltaTime * turnSmoothing);
        }

        handleRotation();


    }

    private void OnAnimatiorMove()
    {
        animator.SetFloat("Speed", currentSpeed);


        if (currentWaitingTime >= idleWaitingTime)
        {
            if (Random.Range(0, 99) < 30)
            {
                animator.SetTrigger("IdleWait");
            }
            currentWatingTime = 0;
        }
    }


private void FixedUpdate()
{
    handleMovement();

}


    private voide handleMovement()

    {
        float targetSpeed = walkSpeed;
        if (runInput > 0)
        {
            targetSpeed = runSpeed;
        }

        if (hInput != 0 || vInput != 0)
        {
            rigidBody.velocity = new Vector3(hInput, 0, vInput) * targetSpeed;

        }
        else
        {
            rigidBody.velocity = Vector3.zero;
        }
        currentSpeed = rigidBody.velocity.magnitude;

    }
}

